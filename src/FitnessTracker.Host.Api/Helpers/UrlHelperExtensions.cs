using System;
using Microsoft.AspNet.Mvc;

namespace FitnessTracker.Host.Api.Controllers
{
    public static class UrlHelperExtensions
    {
        public static string GetFitnessTrackUrl(this IUrlHelper urlHelper, string id)
        {
            return urlHelper.Action("Get", "FitnessTrack", new { id = id });
        }
        
        public static string GetFitnessTrackGroupUrl(this IUrlHelper urlHelper, string id)
        {
            return urlHelper.Action("Get", "FitnessTrackGroup", new { id = id });
        }
    }
}