using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using FitnessTracker.Infrastructure.Services.Commands;
using FitnessTracker.Infrastructure.Services;
using System.Linq;
using FitnessTracker.Host.Api.Models;
using FitnessTracker.Infrastructure.Models;
using System.Collections.Generic;

namespace FitnessTracker.Host.Api.Controllers
{
    [Route("api/[controller]")]
    public class FitnessTrackGroupController : Controller
    {
        private readonly IFitnessTrackingService _fitnessTrackingService;

        public FitnessTrackGroupController(IFitnessTrackingService fitnessTrackingService)
        {
            _fitnessTrackingService = fitnessTrackingService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var response = new List<FitnessTrackGroupModels.Get>();

            Parallel.ForEach(await _fitnessTrackingService.FindAllAsync(), async x =>
            {
                response.Add(new FitnessTrackGroupModels.Get
                {
                    Id = x.Id,
                    DateTracked = x.DateTracked,
                    TotalTracks = await _fitnessTrackingService.GetNumberOfTracksForGroupAsync(x.Id)
                });
            });

            return Ok(response);
        }


        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var fitnessTrack = await _fitnessTrackingService.FindAsync(id);

            if (fitnessTrack == null)
            {
                return HttpNotFound(id);
            }

            return Ok(fitnessTrack);
        }

        [HttpPost]
        public async Task<IActionResult> Post()
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest();
            }

            var fitnessTrackGroup = await _fitnessTrackingService.CreateAsync(new FitnessTrackCommands.CreateGroup
            {
                DateTracked = DateTime.Now
            });

            return Created(Url.GetFitnessTrackGroupUrl(fitnessTrackGroup.Id), fitnessTrackGroup);
        }
    }
}
