using System.Linq;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Services;
using Microsoft.AspNet.Mvc;

namespace FitnessTracker.Host.Api.Controllers
{
    [Route("api/[controller]")]
    public class WorkoutCategoryController : Controller
    {
        private readonly IFitnessTrackingService _fitnessTrackingService;

        public WorkoutCategoryController(IFitnessTrackingService fitnessTrackingService)
        {
            _fitnessTrackingService = fitnessTrackingService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var workoutCategories = await _fitnessTrackingService.FindWorkoutCategories();

            return Ok(workoutCategories);
        }
    }
}