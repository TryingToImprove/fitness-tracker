using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using FitnessTracker.Host.Api.Models;
using FitnessTracker.Infrastructure;
using FitnessTracker.Infrastructure.Services.Commands;
using FitnessTracker.Infrastructure.Services;

namespace FitnessTracker.Host.Api.Controllers
{
    [Route("api/[controller]")]
    public class FitnessTrackController : Controller
    {
        private readonly IFitnessTrackingService _fitnessTrackingService;

        public FitnessTrackController(IFitnessTrackingService fitnessTrackingService)
        {
            _fitnessTrackingService = fitnessTrackingService;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var fitnessTrack = await _fitnessTrackingService.FindAsync(id);

            if (fitnessTrack == null)
            {
                return HttpNotFound(id.ToString());
            }

            return Ok(fitnessTrack);
        }

        [HttpPost]
        public async Task<IActionResult> Post(FitnessTrackModels.Add model)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest();
            }

            var fitnessTrack = await _fitnessTrackingService.AddTrackAsync(new FitnessTrackCommands.AddTrack
            {
                WorkoutCategoryId = model.WorkoutCategory.Id,
                GroupId = model.GroupId,
                Weight = new Weight
                {
                    Type = model.WeightMeasurement,
                    Amount = model.Weight
                },
                Repetitions = model.Repetitions,
                TimeCreated = DateTime.Now
            });

            return Created(Url.GetFitnessTrackUrl(fitnessTrack.Id), fitnessTrack);
        }
    }
}
