var path = require("path");
var webpack = require("webpack");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var DnxBrowserSyncPlugin = require('webpack-dnx-browsersync');
var RedisServerPlugin = require("./RedisServerPlugin");

// CSS
var autoprefixer = require('autoprefixer');
var precss = require('precss');


var entry = "scripts/app";

var root = path.join(__dirname, "src");

var output = {
    path: path.resolve(__dirname, "wwwroot"),
    filename: "/app.js",
    sourceMapFilename: "debugging/[file].map"
  };

var definePlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify('true'),
});

var plugins = [
  definePlugin,
  new webpack.PrefetchPlugin("react"),
  new HtmlWebpackPlugin({
    title: "Fitness tracker",
    template: 'src/index.ejs'
  }),
  new RedisServerPlugin(),
  new DnxBrowserSyncPlugin()
];

module.exports = {
    entry: entry,
    output: output,
    debug: true,
    plugins: plugins,
    module: {
        loaders: [
        { test: /\.js$/, loader: "babel-loader", exclude: /node_modules/ },
        { test: /\.html$/, loader: "html-loader" },
        { test: /\.css$/, loader: "style-loader!css-loader!postcss-loader" }
        ],
    },
    postcss: function () {
        return [autoprefixer, precss];
    },
    resolve: {
        root: root,
        modulesDirectories: ["web_modules", "node_modules"],
        extensions: ["", ".web.js", ".js", ".jsx", ".css"],
        alias: {
        "fitness-tracker/styles": path.resolve(__dirname, "src/styles"),
        "fitness-tracker": path.resolve(__dirname, "src/scripts/")
        }
    },
    resolveLoader: {
        root: path.join(__dirname, "node_modules")
    }
}
