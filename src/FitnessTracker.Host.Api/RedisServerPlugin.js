var browserSync = require('browser-sync');
var child_process = require('child_process');

function RedisServerPlugin(options) {
  this.options = options || {};
}

RedisServerPlugin.prototype.apply = function(compiler) {
  var self = this;
  var child;

  function startDnx() {
    child = child_process.spawn("redis-server", [], {
        stdio: 'inherit',
        cwd: "C:/ProgramData/chocolatey/bin"
    });
    
    child.on('exit', startDnx);
  }
  
  compiler.plugin('watch-run', function (watching, callback) {
    if(!child) {
      startDnx();
    }

    callback(null, null);
  });
}

module.exports = RedisServerPlugin;