using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FitnessTracker.Data;
using FitnessTracker.Infrastructure.Data;
using FitnessTracker.Infrastructure.Services;
using FitnessTracker.Services;
using FitnessTracker.Storage;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc.Formatters;
using Microsoft.AspNet.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;
using StackExchange.Redis;

namespace FitnessTracker.Host.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            });

            services.AddMvc(options =>
            {
                var jsonFormatter = (JsonOutputFormatter)options.OutputFormatters.FirstOrDefault(x => x.GetType() == typeof(JsonOutputFormatter));

                if (jsonFormatter != null)
                {
                    jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    jsonFormatter.SerializerSettings.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Include;
                }
            });

            services.AddSingleton<ConnectionMultiplexer>(x =>
            {
                var connection = ConnectionMultiplexer.Connect("localhost,allowAdmin=true");

                foreach (var endpoint in connection.GetEndPoints())
                {
                    var server = connection.GetServer(endpoint);
                    server.FlushAllDatabases();
                }

                return connection;
            });
            services.AddScoped<IDatabase>(x => (x.GetService(typeof(ConnectionMultiplexer)) as ConnectionMultiplexer).GetDatabase());
            services.AddScoped<ICachedFitnessStorage, CachedFitnessStorage>();
            services.AddScoped<IFitnessStorage>(x => PersistedFitnessStore.Create("http://localhost:5500", "FitnessTracker"));
            services.AddScoped<IFitnessTrackingService, FitnessTrackingService>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors("AllowAll");
            app.UseIISPlatformHandler();

            app.UseMvc();

            app.Use(async (HttpContext httpcontext, Func<Task> next) =>
            {
                var fileProvider = env.WebRootFileProvider;

                // We should only do this if the file not exists
                var originalPathFileInfo = fileProvider.GetFileInfo(httpcontext.Request.Path);
                if (originalPathFileInfo.Exists)
                {
                    await next();
                    return;
                }

                const string defaultPath = "index.html";

                var contentTypeProvider = new FileExtensionContentTypeProvider();

                string contentType;
                if (!contentTypeProvider.TryGetContentType(defaultPath, out contentType))
                {
                    contentType = "text/plain";
                }

                var fileInfo = fileProvider.GetFileInfo(defaultPath);
                httpcontext.Response.ContentType = contentType;
                httpcontext.Response.ContentLength = fileInfo.Length;

                Stream readStream = fileInfo.CreateReadStream();
                try
                {
                    await readStream.CopyToAsync(httpcontext.Response.Body);
                }
                finally
                {
                    readStream.Dispose();
                }
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();
        }

        // Entry point for the application.
        public static void Main(string[] args) => Microsoft.AspNet.Hosting.WebApplication.Run<Startup>(args);
    }
}
