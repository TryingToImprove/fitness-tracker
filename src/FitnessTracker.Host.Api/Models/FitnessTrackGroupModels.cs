using System;

namespace FitnessTracker.Host.Api.Models
{
    public abstract class FitnessTrackGroupModels
    {
        public class Get
        {
            public string Id { get; set; }
            
            public DateTime DateTracked { get; set; }
            
            public long TotalTracks { get; set; }
        }
    }    
}