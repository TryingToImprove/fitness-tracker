using System;
using System.ComponentModel.DataAnnotations;
using FitnessTracker.Infrastructure;

namespace FitnessTracker.Host.Api.Models
{
    public abstract class FitnessTrackModels
    {
        public class Add
        {
            [Required]
            public string GroupId { get; set; }

            [Required]
            public WorkoutCategory WorkoutCategory { get; set; }

            [Required]
            public double Weight { get; set; }

            [Required]
            public Measurement WeightMeasurement { get; set; }

            [Required]
            public int Repetitions { get; set; }
        }

        public class WorkoutCategory
        {

            [Required]
            public string Id { get; set; }
        }
    }
}