import { changePage, changePageTitle } from "fitness-tracker/ducks/application";
import { batchActions } from "redux-batched-actions";
import * as service from "fitness-tracker/services"
import { browserHistory } from "react-router";

const FITNESS_TRACK_COMPLETED = "fitness-tracker/fitness-tracking/FITNESS_TRACK_COMPLETED"
const FITNESS_TRACK_STARTED = "fitness-tracker/fitness-tracking/FITNESS_TRACK_STARTED"
const ADD_WORKOUT_COMPLETED = "fitness-tracker/fitness-tracking/ADD_WORKOUT_COMPLETED"

const WorkoutReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_WORKOUT_COMPLETED:
      let isPresent = false;
      const nextState = state.map(workout => {
        if (workout.id !== action.payload.id) {
          return workout
        }

        isPresent = true;
        return {...action.payload, lastModified: new Date(action.payload.lastModified)};
      });

      if (!isPresent) {
        nextState.push({...action.payload, lastModified: new Date(action.payload.lastModified)});
      }

      nextState.sort(function(a, b) {
        return b.lastModified - a.lastModified;
      });

      return nextState;

    case FITNESS_TRACK_COMPLETED:
      return [...action.payload.workouts.map(x=> ({...x, lastModified: new Date(x.lastModified)}))];

    default:
      return state;
  }
}

const TrackingReducer = (state = {}, action) => {
  switch (action.type) {
    case FITNESS_TRACK_COMPLETED:
      return {...action.payload, isLoading: false };
    case FITNESS_TRACK_STARTED:
      return {...state, isLoading: true };
    default:
      return state;
  }
}

export default (state = {}, action) => {
  return {
    ...TrackingReducer(state, action),
    workouts: WorkoutReducer(state.workouts, action)
  }
}

// private functions
function createOrGetTracking(fitnessTrackId) {
  if (typeof(fitnessTrackId) !== "undefined") {
    return service.findFitnessTrackGroup(fitnessTrackId);
  }

  return service.createFitnessTrackGroup();
}

function addFitnessTrackCompleted(fitnessTrack) {
  return {
    type: FITNESS_TRACK_COMPLETED,
    payload: fitnessTrack
  }
}

// public
export function startTracking(fitnessTrackId) {
  return dispatch => {
    dispatch(batchActions([{ type: FITNESS_TRACK_STARTED }]));

    // Lets redirect!
    browserHistory.push("group/" + fitnessTrackId);

    createOrGetTracking(fitnessTrackId)
      .then((result) => {
          const { dateTracked } = result;
          const pageTitle = dateTracked.getDate() + "-" + (dateTracked.getMonth()+1) + "-" + dateTracked.getFullYear();

          dispatch(batchActions([changePageTitle(pageTitle), addFitnessTrackCompleted(result)]));
        });
  }
};

export function requestTrackingGroup(fitnessTrackId) {
  return dispatch => {
    dispatch(batchActions([{ type: FITNESS_TRACK_STARTED }]));

    service.findFitnessTrackGroup(fitnessTrackId)
      .then((result) => {
          const { dateTracked } = result;
          const pageTitle = dateTracked.getDate() + "-" + (dateTracked.getMonth()+1) + "-" + dateTracked.getFullYear();

          dispatch(batchActions([changePageTitle(pageTitle), addFitnessTrackCompleted(result)]));
        });
  }
};

export function addWorkout(workout) {
  return {
    type: ADD_WORKOUT_COMPLETED,
    payload: workout
  };
}
