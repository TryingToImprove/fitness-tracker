import { changePage, changePageTitle } from "fitness-tracker/ducks/application";
import { addFitnessTrack } from "fitness-tracker/ducks/fitness-tracking";
import { batchActions } from "redux-batched-actions";
import { browserHistory } from "react-router";
import UrlHelper from "fitness-tracker/services/UrlHelper";
import * as service from "fitness-tracker/services"

import { addWorkout } from "fitness-tracker/ducks/fitness-tracking";

const START_WORKOUT = "fitness-tracker/fitness-createWorkout/START_WORKOUT"
const UPDATE_WORKOUT = "fitness-tracker/fitness-createWorkout/UPDATE_WORKOUT"
const CHANGE_PROGRESS = "fitness-tracker/fitness-createWorkout/CHANGE_PROGRESS"

export default (state = {}, action) => {
  switch (action.type) {
    case START_WORKOUT:
      return {
        groupId: action.payload.groupId,
        progress: action.payload.progress,
        workout: {...action.payload.workout }
      };

    case CHANGE_PROGRESS:
      return {
        ...state,
        progress: action.payload.progress
      };

    case UPDATE_WORKOUT:
      return {
        ...state,
        workout: {...state.workout, ...action.payload.workout }
      };

    default:
      return state;
  }
}

// private functions
function startWorkout(groupId, workout) {
  return {
    type: START_WORKOUT,
    payload: {
      groupId: groupId,
      progress: 0,
      workout
    }
  }
}

function updateWorkout(workout) {
  return {
      type: UPDATE_WORKOUT,
      payload: {
        workout
      }
    }
}

function changeProgress(progress) {
  return {
    type: CHANGE_PROGRESS,
    payload: { progress }
  };
}

// public
export function addWorkoutCategory(workoutCategory) {
  const actions = [
    changeProgress(1),
    changePageTitle("Enter weight"),
    updateWorkout({
      workoutCategory: workoutCategory
    })
  ];

  return batchActions(actions);
}

export function addWorkoutWeight(weight, weightMeasurement) {
  const actions = [
    updateWorkout({ weight, weightMeasurement }),
    changeProgress(2),
    changePageTitle("Enter repetitions")
  ];

  return batchActions(actions);
}

export function addWorkoutRepetitions(repetitions) {
  const actions = [
    updateWorkout({ repetitions }),
    changeProgress(3), // No need?
  ]

  return batchActions(actions);
}

export function createWorkout(workout) {
  return (dispatch) => {
    service.saveWorkout({
        ...workout,
        groupId: workout.id
      })
      .then(
        (success) => {
          dispatch(addWorkout(success));

          browserHistory.replace(UrlHelper.getTrackingGroupUrl(workout.id));
        },
        (error) => console.log(error)
      );
  }
}

export function workoutStarted(groupId) {
  return batchActions([changeProgress(0), changePageTitle("Add new workout"), updateWorkout({ id: groupId })]);
};
