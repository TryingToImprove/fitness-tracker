const FETCH_WORKOUT_CATEGORIES_STARTED = "fitness-tracker/workout-categories/FETCH_WORKOUT_CATEGORIES_STARTED";
const FETCH_WORKOUT_CATEGORIES_COMPLETED = "fitness-tracker/workout-categories/FETCH_WORKOUT_CATEGORIES_COMPLETED";

import * as service from "fitness-tracker/services";
import { batchActions } from "redux-batched-actions";

export default (state = { isLoading: false, workoutCategories: [] }, action) => {
  switch (action.type) {
    case FETCH_WORKOUT_CATEGORIES_STARTED:
      return {...state, isLoading: true};
    case FETCH_WORKOUT_CATEGORIES_COMPLETED:
      return {...state, isLoading: false, workoutCategories: action.workoutCategories };
  }

  return state;
}

export function fetchWorkoutCategories() {
  return (dispatch) => {
    dispatch({ type: FETCH_WORKOUT_CATEGORIES_STARTED });

    service.findWorkoutCategories()
      .then((success) => { dispatch({ type: FETCH_WORKOUT_CATEGORIES_COMPLETED, workoutCategories: success }) });
  }
}
