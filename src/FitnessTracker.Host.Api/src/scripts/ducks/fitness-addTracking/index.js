import { changePage } from "fitness-tracker/ducks/application";
import * as service from "fitness-tracker/services"

const FITNESS_TRACK_COMPLETED = "fitness-tracker/fitness-tracking/FITNESS_TRACK_COMPLETED"
const ADD_FITNESS_TRACK_COMPLETED = "fitness-tracker/fitness-tracking/ADD_FITNESS_TRACK_COMPLETED"

export default (state = {}, action) => {
  switch (action.type) {
    case FITNESS_TRACK_COMPLETED:
      return {...action.payload}
    case ADD_FITNESS_TRACK_COMPLETED:
      const copy = {...state};
      copy.tracks = [...copy.tracks, action.payload];
      return copy;
    default:
      return state;
  }
}

// private functions
function createOrGetTracking(fitnessTrackId) {
  if (typeof(fitnessTrackId) !== "undefined") {
    return service.findFitnessTrackGroup(fitnessTrackId);
  }

  return service.createFitnessTrackGroup();
}

function addFitnessTrackCompleted(fitnessTrack) {
  return {
    type: FITNESS_TRACK_COMPLETED,
    payload: fitnessTrack
  }
}

// public
export function startTracking(fitnessTrackId) {
  return dispatch => {
    createOrGetTracking(fitnessTrackId)
      .then((result) => {
          dispatch(addFitnessTrackCompleted(result));
          dispatch(changePage("VIEW_FITNESS_TRACK"));
        });
  }
};

export function addFitnessTrack(fitnessTrack) {
  return dispatch => {
    service.addFitnessTrack(fitnessTrack)
      .then(
        (success) => dispatch({
          type: ADD_FITNESS_TRACK_COMPLETED,
          payload: success
        }),
        (error) => console.log(error)
      );
  };
}
