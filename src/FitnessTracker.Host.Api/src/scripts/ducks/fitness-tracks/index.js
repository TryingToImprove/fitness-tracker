const FETCH_FITNESS_TRACKS = "fitness-tracker/fitness-fitness-tracks/FETCH_FITNESS_TRACKS";
const FETCH_FITNESS_TRACKS_STARTED = "fitness-tracker/fitness-tracks/FETCH_FITNESS_TRACKS_STARTED"
const CREATE_FITNESS_TRACK_STARTED = "fitness-tracker/fitness-tracks/CREATE_FITNESS_TRACK_STARTED"
const CREATE_FITNESS_TRACK_COMPLETED = "fitness-tracker/fitness-tracks/CREATE_FITNESS_TRACK_COMPLETED"

import * as service from "fitness-tracker/services";
import { browserHistory } from "react-router";
import { batchActions } from "redux-batched-actions";
import UrlHelper from "fitness-tracker/services/UrlHelper";

const fitnessTracksReducer = (state = [], action) => {
  switch (action.type) {
    case FETCH_FITNESS_TRACKS:

      return {...action.payload.reduce((o, v) => {
        o[v.id] = v;
        return o;
      }, {})};

    default:
      return state
  }
}


const loadingReducer = (state = true, action) => {
  switch (action.type) {
    case FETCH_FITNESS_TRACKS_STARTED:
      return true;
    case FETCH_FITNESS_TRACKS:
      return false;
    default:
      return state;
  }
}

const createTrackingGroupReducer = (state = false, action) => {
  switch (action.type) {
    case CREATE_FITNESS_TRACK_STARTED:
      return true;
    case CREATE_FITNESS_TRACK_COMPLETED:
      return false;
    default:
      return state;
  }
}

export default (state = {}, action) => {
  return {
    isCreatingTrackingGroup: createTrackingGroupReducer(state.isCreatingTrackingGroup, action),
    items: fitnessTracksReducer(state.items, action),
    isLoading: loadingReducer(state.isLoading, action)
  }
}

// private functions
function fetchFitnessTracksStarted() {
  return {
    type: FETCH_FITNESS_TRACKS_STARTED
  };
}

// public
export function fetchFitnessTracks(fitnessTracks) {
  return {
    type: FETCH_FITNESS_TRACKS,
    payload: fitnessTracks
  }
};

export function fetchFitnessTracksAsync() {
  return dispatch => {
    dispatch(fetchFitnessTracksStarted());

    service.findGroups()
      .then(result => dispatch(fetchFitnessTracks(result)))
  }
};


export function createTrackingGroup() {
  return dispatch => {
    dispatch(batchActions([{ type: CREATE_FITNESS_TRACK_STARTED }]));

    service.createFitnessTrackGroup()
      .then((result) => {
          dispatch(batchActions([{ type: CREATE_FITNESS_TRACK_COMPLETED }]));

          // Redirect the user
          browserHistory.push(UrlHelper.getTrackingGroupUrl(result.id));
        });
  }
};
