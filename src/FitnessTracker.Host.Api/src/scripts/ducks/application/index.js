const PAGE_CHANGED = "fitness-tracker/application/PAGE_CHANGED";
const PAGE_TITLE_CHANGED = "fitness-tracker/application/PAGE_TITLE_CHANGED";

const initialValue = {
  currentPageType: "DEFAULT",
  currentPageTitle: "No title"
};

export default (state = initialValue, action) => {
  switch (action.type) {
    case PAGE_CHANGED:
      return {
        ...state,
        currentPageType: action.payload.pageName
      };
    case PAGE_TITLE_CHANGED:
      return {
        ...state,
        currentPageTitle: action.payload.pageTitle
      };
    default:
      return state;
  }
}

export function changePage(pageName) {
  return {
    type: PAGE_CHANGED,
    payload: {
      pageName
    }
  }
};

export function changePageTitle(pageTitle) {
  return {
    type: PAGE_TITLE_CHANGED,
    payload: {
      pageTitle
    }
  }
};
