export default {
  getTrackingGroupUrl: (trackingGroupId) => (`/group/${trackingGroupId}`),
  getAddTrackingItemUrl: (trackingGroupId) => (`/group/${trackingGroupId}/add-tracking`),
  getTrackOverviewUrl: () => `/`
};
