
import { baseApiHost } from "fitness-tracker/configuration";

const toFormData = function(obj, form, namespace) {

  var fd = form || new FormData();
  var formKey;

  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {

      if(namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }

      // if the property is an object, but not a File,
      // use recursivity.
      if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

        toFormData(obj[property], fd, property);

      } else {

        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }

    }
  }

  return fd;

};

export function findFitnessTrackGroup(fitnessTrackGroupId) {
  return fetch(`${baseApiHost}api/FitnessTrackGroup/${fitnessTrackGroupId}`)
            .then(response => response.json())
            .then(result => {
                result.dateTracked = new Date(result.dateTracked);

                return result;
            });
}

export function createFitnessTrackGroup() {
  return fetch(`${baseApiHost}api/FitnessTrackGroup/`, {
      method: "post",
      body: new FormData({
        // Empty
      })
    })
    .then(response => response.json())
    .then(result => {
        result.dateTracked = new Date(result.dateTracked);

        return result;
    })
}

export function saveWorkout(fitnessTrack) {
  return fetch(`${baseApiHost}api/FitnessTrack/`, {
      method: "post",
      body: toFormData(fitnessTrack)
    })
    .then(response => response.json());
}

export function findGroups() {
  return fetch(`${baseApiHost}api/FitnessTrackGroup`)
    .then((response) => response.json())
    .then(result => result.map(fitnessTrack => {
      fitnessTrack.dateTracked = new Date(fitnessTrack.dateTracked);

      return fitnessTrack;
    }));
}

export function findWorkoutCategories() {
  return fetch(`${baseApiHost}api/WorkoutCategory`)
    .then((response) => response.json());
}
