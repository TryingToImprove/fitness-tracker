import { combineReducers } from "redux";
import { routerReducer } from 'react-router-redux'

// duck duck duck
import application from "fitness-tracker/ducks/application";
import fitnessTracking from "fitness-tracker/ducks/fitness-tracking";
import fitnessTracks from "fitness-tracker/ducks/fitness-tracks";
import workoutCategories from "fitness-tracker/ducks/workout-categories";
import createWorkout from "fitness-tracker/ducks/fitness-createWorkout";

const FitnessTrackerApp = combineReducers({
  application,
  fitnessTracks,
  tracking: fitnessTracking,
  workoutCategories,
  createWorkout,
  routing: routerReducer
});

export default FitnessTrackerApp;
