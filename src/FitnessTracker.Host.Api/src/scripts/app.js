import "flexboxgrid";
import "fitness-tracker/styles/base"
import "whatwg-fetch"; // polyfill
import Promise from "es6-promise";

if (typeof(window.Promise) === "undefined") {
  window.Promise = Proisme
}


import React from "react";
import ReactDOM from "react-dom";

import { createStore, compose, applyMiddleware, combineReducers } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { enableBatching } from "redux-batched-actions";

import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import FitnessTrackerApp from "fitness-tracker/reducers/FitnessTrackerApp";

// Containers
import DevToolsContainer from "fitness-tracker/containers/DevToolsContainer";
import AppContainer from "fitness-tracker/containers/AppContainer";

const storeEnhancer = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

const store = createStore(enableBatching(FitnessTrackerApp), undefined, storeEnhancer);
const history = syncHistoryWithStore(browserHistory, store)



import FitnessTracksContainer from "fitness-tracker/containers/FitnessTracksContainer";
import FitnessTrackItemContainer from "fitness-tracker/containers/FitnessTrackItemContainer";
import AddWorkoutContainer from "fitness-tracker/containers/AddWorkoutContainer";


ReactDOM.render((
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={AppContainer}>
          <IndexRoute component={FitnessTracksContainer}/>
          <Route path="/group/:id/add-tracking" component={AddWorkoutContainer}/>
          <Route path="/group/:id" component={FitnessTrackItemContainer}/>
        </Route>
      </Router>
    </Provider>
  ), document.getElementById("app-container"));
