import React from "react";
import { connect } from "react-redux";
import classnames from "classnames";

// Helpers
import UrlHelper from "fitness-tracker/services/UrlHelper";

// Components
import { Link } from "react-router";
import CalendarDateComponent from "fitness-tracker/components/CalendarDateComponent";
import AddFitnessTrackFormComponent from "fitness-tracker/components/AddFitnessTrackFormComponent";

// Actions
import { changePageTitle } from "fitness-tracker/ducks/application";
import { addFitnessTrack, requestTrackingGroup } from "fitness-tracker/ducks/fitness-tracking";
import { beginWorkoutCreation } from "fitness-tracker/ducks/fitness-createWorkout";

const BeginWorkoutActionBar = (props) => (
  <div className={classnames("actionbar content-container content-container--include-top content-container--include-bottom")}>
    <Link  to={UrlHelper.getAddTrackingItemUrl(props.trackingGroupId)} className={classnames("btn btn--success btn--large btn--block")}>
      Add workout
    </Link>
  </div>
)

const WorkoutRepetitionItem = (props) => (
  <div>
    Weight: {props.weight.amount} {props.weight.type == 1 ? 'kg' : 'lb'}, Repetitions: {props.repetitions}
  </div>
)

const WorkoutItem = (props) => (
  <a href="#" className="item-list__item">
    <div className="item-list__item__content">
      <span className="item-list__item__content__header">
        {props.workoutCategory.name}
      </span>
      <div className="item-list__item__content__description">
        {props.repetitions.map((repetition, i) => (<WorkoutRepetitionItem key={`WorkoutRepetition-${i}`} {...repetition} />))}
      </div>
    </div>
  </a>
)

const WorkoutList = (props) => (
  <div className="item-list">
    <div className="item-list__item item-list__item--header">
      Tracked:
    </div>

    {props.workouts.map(x => (<WorkoutItem key={x.id} {...x} />))}
  </div>
)

const Content = (props) => {
  if (props.isLoading) {
    return (
      <div className="content-container">
        Loader...
      </div>
    );
  }

  if (props.workouts.length == 0) {
    return (
      <div className="content-container">
        No workours registered
      </div
    >);
  }

  return (<WorkoutList workouts={props.workouts} />);
}

class AppContainer extends React.Component {
  componentWillMount() {
    const { onContainerLoaded, params } = this.props;

    onContainerLoaded(params.id);
  }

  render() {
    const { fitnessTrack, onBeginWorkout } = this.props;
    const { isLoading } = fitnessTrack;

    return (
      <div className="layout__flex">
        <BeginWorkoutActionBar trackingGroupId={fitnessTrack.id} />

        <div style={{ flex: 1, overflow: "auto" }}>
          <Content isLoading={isLoading} workouts={Object.keys(fitnessTrack.workouts).map((x) => fitnessTrack.workouts[x])} />
        </div>

        {/*<div className="actionbar__buttons">
          <button type="button" onClick={() => { throw new Error("Not implanted") }} className="actionbar__buttons__button btn btn--block btn--success btn--large">
            Complete workout
          </button>
        </div>*/}
      </div>
    );
  }
}

export default connect(
  (state) => { return {
      fitnessTrack: state.tracking
    };
  },
  (dispatch) => { return {
      onContainerLoaded: (groupId) => dispatch(requestTrackingGroup(groupId)),
      onBeginWorkout: (groupId, name) => dispatch(beginWorkoutCreation(groupId)),
      onAddFitnessTrack: (fitnessTrack) => dispatch(addFitnessTrack(fitnessTrack)),
      onPageTitleChanged: (pageTitle) => dispatch(changePageTitle(pageTitle))
    }
  }
)(AppContainer)
