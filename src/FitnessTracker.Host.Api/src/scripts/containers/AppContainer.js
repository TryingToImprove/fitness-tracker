import React from "react";
import { connect } from "react-redux";
import { Router, Route, browserHistory } from 'react-router'

import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import DevToolsContainer from "fitness-tracker/containers/DevToolsContainer";

// Actions
import { changePage, changePageTitle } from "fitness-tracker/ducks/application";

class AppContainer extends React.Component {
  render() {
    return (
      <div className="app-container">
        <div key="pageTitle" className={"appbar appbar--default"}>
          <span>{this.props.currentPageTitle}</span>
        </div>

        <div className="app-layout-content">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => { return { currentPage: state.application.currentPageType, currentPageTitle: state.application.currentPageTitle } },
  (dispatch) => { return {
      onChangePage: (pageName) => dispatch(changePage(pageName)),
      onPageTitleChanged: (pageTitle) => dispatch(changePageTitle(pageTitle))
    }
  }
)(AppContainer)
