import React from "react";
import { connect } from "react-redux";
import classnames from "classnames";

// Actions
import { fetchWorkoutCategories } from "fitness-tracker/ducks/workout-categories"
import { addWorkoutCategory, changeWorkoutWeight, addWorkoutWeight, addWorkoutRepetitions, createWorkout, workoutStarted } from "fitness-tracker/ducks/fitness-createWorkout";

// Components
import ListSelector from "fitness-tracker/components/ListSelectorComponent";
import NumberComponent from "fitness-tracker/components/NumberComponent"

class CustomNumberComponent extends React.Component {
  constructor(props) {
    super(props);

    this.handleSave = this.handleSave.bind(this);
  }

  handleSave() {
    const parameters = [this.refs.number.state.currentValue];

    if (this.props.type === "weight") {
        parameters.push(this.refs.number.state.weightType)
    }

    this.props.onSave.apply(this, parameters);
  }

  render() {
    return (
      <div className="layout__flex actionbar actionbar-fullscreen">
        <NumberComponent ref="number" defaultValue={this.props.defaultValue} numberType={this.props.type} disableDecimals={this.props.disableDecimals} />

        <div className="actionbar__buttons">
          <button type="button" onClick={() => { throw new Error("Not implanted") }} className="actionbar__buttons__button btn btn--block btn--danger btn--large">
            Cancel
          </button>
          <button type="button" onClick={() => this.handleSave()} className="actionbar__buttons__button btn btn--block btn--success btn--large">
            {this.props.buttons.successText}
          </button>
        </div>
      </div>
    );
  }
}

const LoadingComponent = () => (
  <div className="layout__flex actionbar actionbar-fullscreen">
    <b>Gemmer</b>
  </div>
);

const SelectWorkoutCategoryComponent = (props) => {
  const { isLoading, workoutCategories } = props;

  if (isLoading) {
    return (<div>Loader</div>);
  }

  if (workoutCategories.length === 0) {
    return (<div>No workout categories</div>)
  }

  return (
    <div>
      <ListSelector items={workoutCategories} onItemSelected={(workoutType) => props.handleWorkoutTypeSelected(workoutType)} />
    </div>
  );
}

class AddWorkoutContainer extends React.Component {
  constructor(props) {
    super(props);

    this.handleWorkoutTypeSelected = this.handleWorkoutTypeSelected.bind(this);
    this.handleWorkoutWeightAdded = this.handleWorkoutWeightAdded.bind(this);
    this.handleWorkoutRepetitionsChanged = this.handleWorkoutRepetitionsChanged.bind(this);
  }

  componentWillMount() {
    const { params } = this.props;

    this.props.ensureWorkoutCategories();
    this.props.onWorkoutAdded(params.id);
  }

  handleWorkoutTypeSelected(workoutCategory) {
    this.props.onWorkoutCategoryAdded(workoutCategory);
  }

  handleWorkoutWeightAdded(workoutWeight, workoutWeightType) {
    this.props.onWorkoutWeightAdded(workoutWeight, workoutWeightType);
  }

  handleWorkoutRepetitionsChanged(workoutRepetitions) {
    this.props.onWorkoutRepetitionsAdded(workoutRepetitions);

    // Yeah, let's save the work! ... out ;)
    this.props.onWorkoutCompleted({
      ...this.props.workout,
      repetitions: workoutRepetitions // because there is no redraw, the props is not altered.. Use `this.props.workout.repetitions` if it is not here the event should be triggered
    });
  }

  render() {

    const { progress } = this.props;

    if (typeof(progress) === "undefined" || progress === 0 ) {
      return (<SelectWorkoutCategoryComponent {...this.props.workoutCategories} handleWorkoutTypeSelected={this.handleWorkoutTypeSelected} />)
    }

    if (progress === 1) {
      return (
        <CustomNumberComponent key="weight" type="weight" buttons={{successText: "Continue"}} defaultValue={this.props.weight} onSave={(weight, weightType) => this.handleWorkoutWeightAdded(weight, weightType)} />
      );
    }

    if (progress === 2) {
      return (
        <CustomNumberComponent key="repetitions" disableDecimals={true} buttons={{successText: "Save workout"}} defaultValue={this.props.repetitions} onSave={(repetitions) => this.handleWorkoutRepetitionsChanged(repetitions)} />
      );
    }

    if (progress === 3) {
      return (
        <LoadingComponent />
      );
    }

    // Noi progress? fuck you
    return null;
  }
}

export default connect(
  (state) => ({
    progress: state.createWorkout.progress,
    workout: {
      ...state.createWorkout.workout
    },
    workoutCategories: state.workoutCategories
  }),
  dispatch => ({
    ensureWorkoutCategories: () => dispatch(fetchWorkoutCategories()),
    onWorkoutAdded: (trackingGroupId) => dispatch(workoutStarted(trackingGroupId)),
    onWorkoutCategoryAdded: (workoutCategory) => dispatch(addWorkoutCategory(workoutCategory)),
    onWorkoutWeightAdded: (weight, measurement) => dispatch(addWorkoutWeight(weight, measurement)),
    onWorkoutRepetitionsAdded: (repetitions) => dispatch(addWorkoutRepetitions(repetitions)),
    onWorkoutCompleted: (groupId, workout) => dispatch(createWorkout(groupId, workout))
  })
)(AddWorkoutContainer);
