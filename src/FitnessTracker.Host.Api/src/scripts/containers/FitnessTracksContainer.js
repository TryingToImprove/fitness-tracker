import React from "react";
import { connect } from "react-redux";

// Components
import CalendarDateComponent from "fitness-tracker/components/CalendarDateComponent";
import { Link } from "react-router";

// Helpers
import UrlHelper from "fitness-tracker/services/UrlHelper";

// Actions
import { changePageTitle, changePage } from "fitness-tracker/ducks/application";
import { fetchFitnessTracksAsync, createTrackingGroup } from "fitness-tracker/ducks/fitness-tracks";

const TrackItemComponent = (props) => (
  <Link to={UrlHelper.getTrackingGroupUrl(props.id)} className="item-list__item">
    <div className="item-list__item__image">
      <CalendarDateComponent date={props.dateTracked} />
    </div>
    <div className="item-list__item__content">
      <div className="item-list__item__content__header">
        {props.dateTracked.getDate() + "-" + (props.dateTracked.getMonth()+1) + "-" + props.dateTracked.getFullYear()}
      </div>
      <span>
        Number of tracks: {props.totalTracks}
      </span>
    </div>
  </Link>
);

class AppContainer extends React.Component {

  componentWillMount() {
    this.props.onPageTitleChanged("Fitness tracks");
    this.props.onFetchFitnessTracks();
  }

  render() {
    const { isLoading, items, onStartTracking, isCreatingTrackingGroup } = this.props;
    const fitnessTracks = items;

    if (isLoading) {
      return (
        <div className="content-container">
          Loader...
        </div>
      );
    }

    if (isCreatingTrackingGroup) {
      return (
        <div className="content-container">
          Wait, while we create a new tracking group...
        </div>
      );
    }

    return (
      <div>
        <div className="actionbar content-container content-container--include-top content-container--include-bottom">
          <a href="#" onClick={(e) => { e.preventDefault(); onStartTracking()}} className="btn btn--large btn--block btn--success">Start new tracking</a>
        </div>

        <div className="item-list">
          {Object.keys(fitnessTracks).map((x) => (<TrackItemComponent onClick={(fitnessTrackId) => onStartTracking(fitnessTrackId)} key={fitnessTracks[x].id} {...fitnessTracks[x]} />))}
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => {
    return { ...state.fitnessTracks }
  },
  (dispatch) => { return {
      onPageTitleChanged: (pageTitle) => dispatch(changePageTitle(pageTitle)),
      onFetchFitnessTracks: () => dispatch(fetchFitnessTracksAsync()),
      onStartTracking: (fitnessTrackId) => dispatch(createTrackingGroup(fitnessTrackId))
    }
  }
)(AppContainer)
