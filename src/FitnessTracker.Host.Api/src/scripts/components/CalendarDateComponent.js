import React from "react";

// Refactor this for localization
const shortMonths = {
  0: "Jan",
  1: "Feb",
  2: "Mar",
  3: "Apr",
  4: "May",
  5: "June",
  6: "July",
  7: "Aug",
  8: "Sept",
  9: "Oct",
  10: "Nov",
  11: "Dec"
}

export default function(props) {
  return (
    <time className="calendar-date">
      <strong className="calendar-date__month">{shortMonths[props.date.getMonth()]}</strong>
      <span className="calendar-date__date">{props.date.getDate()}</span>
    </time>
  )
}
