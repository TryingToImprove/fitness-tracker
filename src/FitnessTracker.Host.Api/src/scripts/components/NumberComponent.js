import React from "react";

const PossibleWeightTypes = ["kilos", "pounds"];

const WeightResult = (props) => {

  const weightText = props.weightType === "kilos"
                        ? "kilos"
                        : "pounds";

  const handleWeightTypeChange = () => {
      const currentWeightTypeIndex = PossibleWeightTypes.indexOf(props.weightType);
      const totalWeightTypes = PossibleWeightTypes.length;
      const nextWeightTypeIndex = (currentWeightTypeIndex + 1) % totalWeightTypes

      props.onWeightTypeChanged(PossibleWeightTypes[nextWeightTypeIndex]);
  }

  return (
    <div className="numberpad__result__container">
        <span className="numberpad__result">{props.currentValue}</span>
        <span className="numberpad__result__placeholder" onClick={(e) => { e.preventDefault(); handleWeightTypeChange() }}>{` ${weightText}`}</span>
    </div>
  );
}

const DefaultResult = (props) => (
  <div className="numberpad__result__container">
      <span className="numberpad__result">{props.currentValue}</span>
  </div>
)

const createNumberArray = (disableDecimals) => {
    const numberArr = Array.apply(null, {length: 9})
                        .map(Number.call, Number)
                        .map(i => i + 1);

    if (typeof(disableDecimals) === "undefined" || !disableDecimals) {
      numberArr.push(".");
    }

    numberArr.push(0);

    return numberArr;
}

class NumberComponent extends React.Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleWeightTypeChange = this.handleWeightTypeChange.bind(this);

    this.state = {
      currentValue: props.defaultValue || 0,
      buttons: createNumberArray(props.disableDecimals),
      weightType: PossibleWeightTypes[0]
    };
  }

  handleWeightTypeChange(weightType) {
    if (PossibleWeightTypes.indexOf(weightType) < 0) {
      throw new Error(`"${weightType}" is not a allowed weight type`);
    }

    this.setState({ weightType });
  }

  handleButtonClick(buttonValue) {console.log("TESt")
    const { currentValue, isDecimalSeperated } = this.state
    const isDecimalSeperator = buttonValue === "."; // Check if the `buttonValue` is a seperator

    // If we are already seperated with decimals, we just stop executing
    if (isDecimalSeperator && isDecimalSeperated) {
      return;
    }

    // Let's update the number
    let newValue = parseFloat(currentValue.toString() + buttonValue, 10);

    if (typeof(this.props.onChange) === "function") {
      this.props.onChange(newValue);
    }

    // We need to append the "decimal"-seperator if that is the `buttonValue`
    if (isDecimalSeperator) {
        newValue = newValue + ".";
    }

    this.setState({
      currentValue: newValue,
      isDecimalSeperated: isDecimalSeperated || isDecimalSeperator
    });
  }

  renderResult() {
    const { numberType } = this.props;

    if (numberType === "weight") {
      return <WeightResult {...this.props} {...this.state} onWeightTypeChanged={this.handleWeightTypeChange} />
    }

    return <DefaultResult {...this.props} {...this.state} />
  }

  render() {
    const { buttons } = this.state;

    return (
      <div className="numberpad numberpad--dark">
        {this.renderResult()}

        <div className="numberpad__container">
          {buttons.map(number => (
            <button
              key={`numberpad-button--${number}`}
              onTouchEnd={() => this.handleButtonClick(number)}
              className={`numberpad__button numberpad_button--${number}`}
              type="button">{number}</button>
          ))}
        </div>
      </div>
    )
  }
}

export default NumberComponent;
