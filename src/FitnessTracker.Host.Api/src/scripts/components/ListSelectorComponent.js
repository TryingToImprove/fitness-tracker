import React from "react";

class ListSelector extends React.Component {
  static propTypes = {
    onItemSelected: React.PropTypes.func.isRequired,
    items: React.PropTypes.array.isRequired
  };

  render() {
    const { onItemSelected } = this.props;
    const items = this.props.items.map(item => (
      <a onClick={(e) => { e.preventDefault(); onItemSelected(item); }} key={item.name} className="item-list__item">
        {item.name}
      </a>
    ));

    return (
      <div className="item-list">
        {items}
      </div>
    )
  }
};

export default ListSelector;
