import React from "react";

import NumberComponent from "fitness-tracker/components/NumberComponent"

const NameProgressComponent = (props) => {
  let name;

  return (
    <div className="row">
      <div className="col-xs-9">
        <input type="text" defaultValue={props.currentValue} className="form-control" placeholder="Enter track name" ref={(node) => name = node} />
      </div>
      <div className="col-xs-3">
        <button type="button" className="btn btn--large btn--block btn--success" onClick={e => { e.preventDefault(); props.onProgressCompleted({ name: name.value }) }}>
          Begin
        </button>
      </div>
    </div>
  );
}

class WeightProgressComponent extends React.Component {
  constructor(props) {
    super(props);

    this.handleWeightChanged = this.handleWeightChanged.bind(this);

    this.state = {
      weight: 0
    };
  }

  handleWeightChanged(num) {
    this.setState({
      weight: num
    });
  }

  render() {
    return (
      <div className="layout__flex">
        <NumberComponent onChange={this.handleWeightChanged}>
          <div className="numberpad__result__container">
              <span className="numberpad__result">{this.state.weight}</span>
              <span className="numberpad__result__placeholder">{' '}kg</span>
          </div>
        </NumberComponent>

        <div className="row">
          <div className="col-xs-6">
            <button type="button" onClick={this.props.onProgressCancelled} className="btn btn--block btn--danger btn--large">
              Cancel
            </button>
          </div>
          <div className="col-xs-6">
            <button type="button" onClick={() => this.props.onProgressCompleted({ weight: this.state.weight })} className="btn btn--block btn--success btn--large">
              Save workout
            </button>
          </div>
        </div>
      </div>
    );
  }
}

class RepitationProgressComponent extends React.Component {
  constructor(props) {
    super(props);

    this.handleWeightChanged = this.handleWeightChanged.bind(this);

    this.state = {
      repitations: 0
    };
  }

  handleRepitationsChanged(num) {
    this.setState({
      repitations: num
    });
  }

  render() {
    return (
      <div className="layout__flex">
        <NumberComponent onChange={this.handleRepitationsChanged}>
          <div className="numberpad__result__container">
              <span className="numberpad__result">{this.state.repitations}</span>
              <span className="numberpad__result__placeholder">{' '}kg</span>
          </div>
        </NumberComponent>

        <div className="row">
          <div className="col-xs-6">
            <button type="button" onClick={this.props.onProgressCancelled} className="btn btn--block btn--danger btn--large">
              Cancel
            </button>
          </div>
          <div className="col-xs-6">
            <button type="button" onClick={() => this.props.onProgressCompleted({ weight: this.state.repitations})} className="btn btn--block btn--success btn--large">
              Save workout
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const getDefaultFitnessTrackState = () => {
  return {
    name: "",
    weight: 0
  };
}

class AddFitnessTrackFormComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = getDefaultFitnessTrackState();

    this.handleProgressCancelled = this.handleProgressCancelled.bind(this);
  }

  handleProgressCompleted(progress, data) {
    const nextState = { ...this.state, ...data };
    const nextProgress = this.props.progress + 1;

    this.props.onProgressChanged(nextProgress);

    if (nextProgress === 2) {
      this.setState(getDefaultFitnessTrackState());
      this.props.onSave(nextState);
    } else {
      this.setState(nextState);
    }
  }

  handleProgressCancelled(progress) {
    this.props.onProgressChanged(progress - 1);
  }

  render() {
    return (
      <form className="layout__flex" onSubmit={this.handleFormSubmit}>
        {this.renderCurrentProgress(this.props.progress)}
      </form>
    );
  }

  renderCurrentProgress(progress) {
    if (progress === 0) {
      return (<NameProgressComponent currentValue={this.state.name} onProgressCompleted={(data) => this.handleProgressCompleted(progress, data)} />);
    }

    if (progress === 1) {
      return (<WeightProgressComponent onProgressCompleted={(data) => this.handleProgressCompleted(progress, data)} onProgressCancelled={() => this.handleProgressCancelled(progress)} />);
    }
  }
}

export default AddFitnessTrackFormComponent;
