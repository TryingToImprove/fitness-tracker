using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Data;
using FitnessTracker.Infrastructure.Models;
using Newtonsoft.Json;
using StackExchange.Redis;


namespace FitnessTracker.Data
{
    public class CachedFitnessStorage : ICachedFitnessStorage
    {
        private const string FitnessTrackGroupsRedisKey = "FitnessTrackGroups";

        private const string WorkoutTypesCacheKey = "WorkoutTypes";

        private readonly IDatabase _database;

        public CachedFitnessStorage(IDatabase database)
        {
            _database = database;
        }

        public async Task<CacheResult<FitnessTrackGroup>> FindAsync(string id)
        {
            return await FindImplAsync(id);
        }

        public Task SaveWorkoutAsync(string fitnessTrackGroupId, Workout workout)
        {
            var trackGroupId = $"{fitnessTrackGroupId}:tracks";
            var trackId = $"{workout.Id}";

            var transaction = _database.CreateTransaction();
            transaction.StringSetAsync(trackId, JsonConvert.SerializeObject(workout));
            transaction.ListRemoveAsync(trackGroupId, trackId);
            transaction.ListRightPushAsync(trackGroupId, trackId);

            return transaction.ExecuteAsync();
        }

        public Task AddAsync(FitnessTrackGroup fitnessTrackGroup)
        {
            var groupId = fitnessTrackGroup.Id;

            var transaction = _database.CreateTransaction();
            transaction.StringSetAsync(groupId, JsonConvert.SerializeObject(fitnessTrackGroup));
            transaction.ListRightPushAsync(FitnessTrackGroupsRedisKey, groupId);

            return transaction.ExecuteAsync();
        }

        public Task UpdateAsync(FitnessTrackGroup fitnessTrackGroup)
        {
            var groupId = fitnessTrackGroup.Id;

            var transaction = _database.CreateTransaction();
            transaction.StringSetAsync(groupId, JsonConvert.SerializeObject(fitnessTrackGroup));

            return transaction.ExecuteAsync();
        }

        public async Task<IEnumerable<CacheResult<WorkoutCategory>>> FindWorkoutCategoriesAsync()
        {
            if (!await _database.KeyExistsAsync(WorkoutTypesCacheKey))
            {
                return null;
            }

            return await _database.ListRangeAsync(WorkoutTypesCacheKey).ContinueWith(x =>
            {
                return x.Result.Select(y => FindWorkoutCategoryImplAsync(y).Result);
            });
        }

        public async Task<IEnumerable<CacheResult<FitnessTrackGroup>>> FindAllAsync()
        {
            if (!await _database.KeyExistsAsync(FitnessTrackGroupsRedisKey))
            {
                return null;
            }

            return await _database.ListRangeAsync(FitnessTrackGroupsRedisKey).ContinueWith(x =>
            {
                return x.Result.Select(y => FindImplAsync(y).Result);
            });
        }

        public async Task<IEnumerable<CacheResult<Workout>>> FindWorkoutsAsync(string fitnessTrackGroupId)
        {
            var trackGroupId = $"{fitnessTrackGroupId}:tracks";

            if (!await _database.KeyExistsAsync(trackGroupId))
            {
                return null;
            }

            return await _database.ListRangeAsync(trackGroupId).ContinueWith(x =>
            {
                return x.Result.Select(y => FindWorkoutImplAsync(y).Result);
            });
        }

        public Task<long> GetNumberOfTracksForGroupAsync(string fitnessTrackGroupId)
        {
            var trackGroupId = $"{fitnessTrackGroupId}:tracks";

            return _database.ListLengthAsync(trackGroupId);
        }

        private async Task<CacheResult<WorkoutCategory>> FindWorkoutCategoryImplAsync(string workoutCategoryId)
        {
            var workoutCategory = await _database.StringGetAsync(workoutCategoryId);

            if (workoutCategory == RedisValue.Null)
            {
                return new CacheResult<WorkoutCategory>
                {
                    Lookup = workoutCategoryId,
                    IsStored = false
                };
            }

            return new CacheResult<WorkoutCategory>
            {
                Lookup = workoutCategoryId,
                IsStored = true,
                Item = JsonConvert.DeserializeObject<WorkoutCategory>(workoutCategory)
            };
        }

        private async Task<CacheResult<FitnessTrackGroup>> FindImplAsync(string fitnessTrackingGroupId)
        {
            var fitnessTrackGroup = await _database.StringGetAsync(fitnessTrackingGroupId);

            if (fitnessTrackGroup == RedisValue.Null)
            {
                return new CacheResult<FitnessTrackGroup>
                {
                    Lookup = fitnessTrackingGroupId,
                    IsStored = false
                };
            }

            return new CacheResult<FitnessTrackGroup>
            {
                Lookup = fitnessTrackingGroupId,
                IsStored = true,
                Item = JsonConvert.DeserializeObject<FitnessTrackGroup>(fitnessTrackGroup)
            };
        }

        private async Task<CacheResult<Workout>> FindWorkoutImplAsync(string workoutId)
        {
            var workoutAsJson = await _database.StringGetAsync(workoutId);

            if (workoutAsJson == RedisValue.Null)
            {
                return new CacheResult<Workout>
                {
                    Lookup = workoutId,
                    IsStored = false
                };
            }

            return new CacheResult<Workout>
            {
                Lookup = workoutId,
                IsStored = true,
                Item = JsonConvert.DeserializeObject<Workout>(workoutAsJson)
            };
        }

        public Task<CacheResult<WorkoutCategory>> FindWorkoutCategoryAsync(string id)
        {
            return FindWorkoutCategoryImplAsync(id);
        }

        public Task AddWorkoutCategoryAsync(WorkoutCategory workoutCategory)
        {
            var workoutCategoryId = workoutCategory.Id;

            var transaction = _database.CreateTransaction();
            transaction.StringSetAsync(workoutCategoryId, JsonConvert.SerializeObject(workoutCategory));
            transaction.ListRightPushAsync(WorkoutTypesCacheKey, workoutCategoryId);

            return transaction.ExecuteAsync();
        }
    }
}
