using System;

namespace FitnessTracker.Infrastructure.Models
{
    public class WorkoutRepetition
    {
        public Weight Weight { get; set; }

        public int Repetitions { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}