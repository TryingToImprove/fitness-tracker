using System;
using System.Collections.Generic;

namespace FitnessTracker.Infrastructure.Models
{
    public class Workout
    {
        public Workout()
        {
            Repetitions = new List<WorkoutRepetition>();
        }

        public string Id { get; set; }

        public WorkoutCategory WorkoutCategory { get; set; }

        public DateTime LastModified { get; set; }
        
        public IList<WorkoutRepetition> Repetitions { get; set; }
    }
}