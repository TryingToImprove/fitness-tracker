using System;
using System.Collections.Generic;

namespace FitnessTracker.Infrastructure.Models
{
    public class FitnessTrackGroup
    {
        public string Id { get; set; }

        public DateTime DateTracked { get; set; }

        public IList<Workout> Workouts { get; set; }
    }
}