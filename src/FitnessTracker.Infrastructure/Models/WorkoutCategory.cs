using System;
using System.Collections.Generic;

namespace FitnessTracker.Infrastructure.Models
{
    public class WorkoutCategory
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}