
using System;
using FitnessTracker.Infrastructure;

namespace FitnessTracker.Infrastructure.Services.Commands
{
    public abstract class FitnessTrackCommands
    {
        public class AddTrack
        {
            public string GroupId { get; set; }

            public string Name { get; set; }

            public Weight Weight { get; set; }
        
            public int Repetitions { get; set; }
            
            public DateTime TimeCreated { get; set; }
            
            public string WorkoutCategoryId { get; set; }
        }

        public class CreateGroup
        {
            public DateTime DateTracked { get; set; }
        }
    }
}