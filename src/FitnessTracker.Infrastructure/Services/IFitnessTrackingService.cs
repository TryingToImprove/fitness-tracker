using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Models;
using FitnessTracker.Infrastructure.Services.Commands;

namespace FitnessTracker.Infrastructure.Services
{
    public interface IFitnessTrackingService
    {
        Task<IEnumerable<FitnessTrackGroup>> FindAllAsync();

        Task<Workout> AddTrackAsync(FitnessTrackCommands.AddTrack command);

        Task<FitnessTrackGroup> CreateAsync(FitnessTrackCommands.CreateGroup command);

        Task<FitnessTrackGroup> FindAsync(string id);

        Task<IList<WorkoutCategory>> FindWorkoutCategories();

        Task<long> GetNumberOfTracksForGroupAsync(string id);
    }
}
