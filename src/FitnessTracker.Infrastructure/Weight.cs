
namespace FitnessTracker.Infrastructure
{
    public struct Weight
    {
        public Measurement Type { get; set; }

        public double Amount { get; set; }
    }
}