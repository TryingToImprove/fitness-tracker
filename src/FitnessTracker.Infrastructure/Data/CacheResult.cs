using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Data;
using FitnessTracker.Infrastructure.Models;


namespace FitnessTracker.Infrastructure.Data
{
    public class CacheResult<T>
    {
        public bool IsStored { get; set; }
        
        public string Lookup { get; set; }

        public T Item { get; set; }
    }
}
