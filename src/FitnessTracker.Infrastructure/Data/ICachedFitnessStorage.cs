using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Models;

namespace FitnessTracker.Infrastructure.Data
{
    public interface ICachedFitnessStorage
    {
        Task<IEnumerable<CacheResult<FitnessTrackGroup>>> FindAllAsync();

        Task AddAsync(FitnessTrackGroup fitnessTrackGroup);

        Task UpdateAsync(FitnessTrackGroup fitnessTrackGroup);

        Task<CacheResult<FitnessTrackGroup>> FindAsync(string fitnessTrackGroupId);

        Task SaveWorkoutAsync(string fitnessTrackGroupId, Workout workout);

        Task<IEnumerable<CacheResult<Workout>>> FindWorkoutsAsync(string fitnessTrackGroupId);

        Task<long> GetNumberOfTracksForGroupAsync(string fitnessTrackGroupId);

        Task<IEnumerable<CacheResult<WorkoutCategory>>> FindWorkoutCategoriesAsync();

        Task<CacheResult<WorkoutCategory>> FindWorkoutCategoryAsync(string id);

        Task AddWorkoutCategoryAsync(WorkoutCategory workoutCategory);
    }
}