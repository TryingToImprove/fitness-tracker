using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Models;

namespace FitnessTracker.Infrastructure.Data
{
    public interface IFitnessStorage
    {
        Task<IList<FitnessTrackGroup>> FindAllAsync();

        Task<FitnessTrackGroup> AddAsync(FitnessTrackGroup fitnessTrackGroup);

        Task UpdateAsync(FitnessTrackGroup fitnessTrackGroup);

        Task<FitnessTrackGroup> FindAsync(string fitnessTrackGroupId);

        Task<IList<FitnessTrackGroup>> FetchAsync(IEnumerable<string> fitnessTrackGroupIds);

        Task SaveWorkoutAsync(string fitnessTrackGroupId, Workout workout);

        Task<IList<Workout>> FindWorkoutsAsync(string fitnessTrackGroupId);

        Task<long> GetNumberOfTracksForGroupAsync(string fitnessTrackGroupId);

        Task<IList<WorkoutCategory>> FindWorkoutCategoriesAsync();

        Task<WorkoutCategory> FindWorkoutCategoryAsync(string id);

        Task<IList<WorkoutCategory>> FetchWorkoutCategoriesAsync(IEnumerable<string> workoutCategoryIds);

        Task<IList<Workout>> FetchWorkoutsAsync(IEnumerable<string> workoutIds);
    }
}