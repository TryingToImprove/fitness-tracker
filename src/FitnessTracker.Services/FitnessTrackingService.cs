using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Data;
using FitnessTracker.Infrastructure.Models;
using FitnessTracker.Infrastructure.Services;
using FitnessTracker.Infrastructure.Services.Commands;

namespace FitnessTracker.Services
{
    public class FitnessTrackingService : IFitnessTrackingService
    {
        private IFitnessStorage _fitnessStorage;
        private ICachedFitnessStorage _cachedFitnessStorage;

        public FitnessTrackingService(IFitnessStorage fitnessStorage, ICachedFitnessStorage cachedFitnessStorage)
        {
            _fitnessStorage = fitnessStorage;
            _cachedFitnessStorage = cachedFitnessStorage;
        }

        public async Task<Workout> AddTrackAsync(FitnessTrackCommands.AddTrack command)
        {
            var fitnessTrackGroup = await FindAsync(command.GroupId);

            if (fitnessTrackGroup == null)
                throw new Exception("Group does not exists");

            // Get or create a workout
            Workout workout = fitnessTrackGroup.Workouts.SingleOrDefault(x => x.WorkoutCategory.Id.Equals(command.WorkoutCategoryId, StringComparison.CurrentCultureIgnoreCase));

            if (workout == null)
            {
                workout = new Workout
                {
                    WorkoutCategory = await FindWorkoutCategory(command.WorkoutCategoryId)
                };

                fitnessTrackGroup.Workouts.Add(workout);
            }


            // The workout is not modified
            // TODO: Find out if it should have a check to see if LastModified is lower than command.TimeCreated, this might cause wrong LastModified dates
            workout.LastModified = command.TimeCreated;

            // Add the workout repetition
            workout.Repetitions.Add(new WorkoutRepetition
            {
                Weight = command.Weight,
                Repetitions = command.Repetitions,
                TimeCreated = command.TimeCreated
            });

            // Save changes
            await _fitnessStorage.SaveWorkoutAsync(fitnessTrackGroup.Id, workout);
            Console.WriteLine($"Workout; With id: {workout.Id}");
            await _cachedFitnessStorage.SaveWorkoutAsync(fitnessTrackGroup.Id, workout);

            return workout;
        }

        private async Task<WorkoutCategory> FindWorkoutCategory(string id)
        {
            WorkoutCategory workoutCategory;

            var workoutCategoryCache = await _cachedFitnessStorage.FindWorkoutCategoryAsync(id);

            if (workoutCategoryCache != null && workoutCategoryCache.IsStored)
            {
                workoutCategory = workoutCategoryCache.Item;
            }
            else
            {
                workoutCategory = await _fitnessStorage.FindWorkoutCategoryAsync(id);

                if (workoutCategory == null)
                {
                    return null;
                }

                await _cachedFitnessStorage.AddWorkoutCategoryAsync(workoutCategoryCache.Item);
            }

            return workoutCategory;
        }

        public async Task<FitnessTrackGroup> CreateAsync(FitnessTrackCommands.CreateGroup command)
        {
            var fitnessTrackGroup = await _fitnessStorage.AddAsync(new FitnessTrackGroup
            {
                DateTracked = command.DateTracked,
                Workouts = new List<Workout>()
            });

            await _cachedFitnessStorage.AddAsync(fitnessTrackGroup);

            return fitnessTrackGroup;
        }

        public async Task<FitnessTrackGroup> FindAsync(string id)
        {
            var fitnessTrackGroupCache = await _cachedFitnessStorage.FindAsync(id);

            Console.WriteLine("fitnessTrackGroupCache");

            if (!fitnessTrackGroupCache.IsStored)
            {
                Console.WriteLine("FitnessTrackGroup is not stored");
                fitnessTrackGroupCache.Item = await _fitnessStorage.FindAsync(id);

                if (fitnessTrackGroupCache.Item == null)
                    return null;

                await _cachedFitnessStorage.AddAsync(fitnessTrackGroupCache.Item);
            }

            fitnessTrackGroupCache.Item.Workouts = await FindWorkoutsAsync(id);

            Console.WriteLine($"Workouts fetched: {fitnessTrackGroupCache.Item.Workouts.Count}");

            return fitnessTrackGroupCache.Item;
        }

        private async Task<IList<Workout>> FindWorkoutsAsync(string id)
        {
            IList<Workout> workouts;

            var workoutsCache = await _cachedFitnessStorage.FindWorkoutsAsync(id);

            if (workoutsCache == null)
            {
                workouts = await _fitnessStorage.FindWorkoutsAsync(id);

                await Task.WhenAll(workouts.Select(x => _cachedFitnessStorage.SaveWorkoutAsync(id, x)));

                return workouts;
            }

            var missingWorkouts = workoutsCache.Where(x => !x.IsStored).ToDictionary(x => x.Lookup, x => x);

            if (missingWorkouts.Any())
            {
                var fetchedWorkouts = await _fitnessStorage.FetchWorkoutsAsync(missingWorkouts.Keys);

                foreach (var workout in fetchedWorkouts)
                {
                    missingWorkouts[workout.Id].Item = workout;
                }

                await Task.WhenAll(fetchedWorkouts.Select(x => _cachedFitnessStorage.SaveWorkoutAsync(id, x)));
            }

            return workoutsCache.Select(x => x.Item).ToList();
        }

        public Task<long> GetNumberOfTracksForGroupAsync(string id)
        {
            return _fitnessStorage.GetNumberOfTracksForGroupAsync(id);
        }

        public async Task<IEnumerable<FitnessTrackGroup>> FindAllAsync()
        {
            IEnumerable<FitnessTrackGroup> fitnessTrackGroups;

            var fitnessTrackGroupsCache = await _cachedFitnessStorage.FindAllAsync();

            Console.WriteLine($"fitnessTrackGroupsCache: {fitnessTrackGroupsCache == null}");

            if (fitnessTrackGroupsCache == null)
            {
                fitnessTrackGroups = await _fitnessStorage.FindAllAsync();

                Console.WriteLine($"fitnessTrackGroups: {fitnessTrackGroups == null} {fitnessTrackGroups.Count()}");

                await Task.WhenAll(fitnessTrackGroups.Select(x => _cachedFitnessStorage.AddAsync(x)));
            }
            else
            {
                var missingFitnessTrackGroups = fitnessTrackGroupsCache.Where(x => !x.IsStored).ToDictionary(x => x.Lookup, x => x);

                Console.WriteLine($"Missing: {missingFitnessTrackGroups.Count}");

                if (missingFitnessTrackGroups.Any())
                {
                    var fetchedFitnessTrackGroups = await _fitnessStorage.FetchAsync(missingFitnessTrackGroups.Keys);

                    foreach (var fitnessTrackGroup in fetchedFitnessTrackGroups)
                    {
                        missingFitnessTrackGroups[fitnessTrackGroup.Id].Item = fitnessTrackGroup;
                    }

                    await Task.WhenAll(fetchedFitnessTrackGroups.Select(x => _cachedFitnessStorage.AddAsync(x)));
                }

                fitnessTrackGroups = fitnessTrackGroupsCache.Select(x => x.Item);
            }

            return fitnessTrackGroups;
        }

        public async Task<IList<WorkoutCategory>> FindWorkoutCategories()
        {
            IList<WorkoutCategory> workoutCategories;

            var workoutCategoriesCache = await _cachedFitnessStorage.FindWorkoutCategoriesAsync();

            if (workoutCategoriesCache == null)
            {
                workoutCategories = await _fitnessStorage.FindWorkoutCategoriesAsync();

                await Task.WhenAll(workoutCategories.Select(x => _cachedFitnessStorage.AddWorkoutCategoryAsync(x)));

                return workoutCategories;
            }

            var missingWorkoutCategories = workoutCategoriesCache.Where(x => !x.IsStored).ToDictionary(x => x.Lookup, x => x);

            if (missingWorkoutCategories.Any())
            {
                var fetchedWorkoutCategories = await _fitnessStorage.FetchWorkoutCategoriesAsync(missingWorkoutCategories.Keys);

                foreach (var workoutCategory in fetchedWorkoutCategories)
                {
                    missingWorkoutCategories[workoutCategory.Id].Item = workoutCategory;
                }

                await Task.WhenAll(fetchedWorkoutCategories.Select(x => _cachedFitnessStorage.AddWorkoutCategoryAsync(x)));
            }

            return workoutCategoriesCache.Select(x => x.Item).ToList();
        }
    }
}
