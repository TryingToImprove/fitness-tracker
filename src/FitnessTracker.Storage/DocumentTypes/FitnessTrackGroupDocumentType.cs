using System;
using System.Collections.Generic;

namespace FitnessTracker.Storage.DocumentTypes
{
    public class FitnessTrackGroupDocumentType
    {
        public string Id { get; set; }

        public DateTime DateTracked { get; set; }

        public IEnumerable<string> Workouts { get; set; }
    }
}