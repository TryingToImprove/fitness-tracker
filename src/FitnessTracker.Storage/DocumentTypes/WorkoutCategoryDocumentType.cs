namespace FitnessTracker.Storage.DocumentTypes
{
    public class WorkoutCategoryDocumentType
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}