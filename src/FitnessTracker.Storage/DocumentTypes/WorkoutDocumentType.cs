using System;
using System.Collections.Generic;
using FitnessTracker.Infrastructure.Models;

namespace FitnessTracker.Storage.DocumentTypes
{
    public class WorkoutDocumentType
    {
        public string Id { get; set; }
        
        public string FitnessTrackGroupId { get; set; }

        public string WorkoutCategory { get; set; }

        public DateTime LastModified { get; set; }

        public IList<WorkoutRepetition> Repetitions { get; set; }
    }
}