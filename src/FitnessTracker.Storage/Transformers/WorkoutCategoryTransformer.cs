using System.Linq;
using FitnessTracker.Infrastructure.Models;
using FitnessTracker.Storage.DocumentTypes;
using Raven.Client.Indexes;

namespace FitnessTracker.Storage
{
    public class WorkoutCategoryTransformer : AbstractTransformerCreationTask<WorkoutCategoryDocumentType>
    {
        public WorkoutCategoryTransformer()
        {
            TransformResults = workoutCategories => from workoutCategory in workoutCategories
                                                    select new WorkoutCategory
                                                    {
                                                        Id = workoutCategory.Id,
                                                        Name = workoutCategory.Name
                                                    };
        }
    }
}