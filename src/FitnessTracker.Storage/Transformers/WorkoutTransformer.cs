using System.Linq;
using FitnessTracker.Infrastructure.Models;
using FitnessTracker.Storage.DocumentTypes;
using Raven.Client.Indexes;

namespace FitnessTracker.Storage
{
    public class WorkoutTransformer : AbstractTransformerCreationTask<WorkoutDocumentType>
    {
        public WorkoutTransformer()
        {
            TransformResults = workouts => from workout in workouts
                                           let workoutCategory = Include<WorkoutCategoryDocumentType>(workout.WorkoutCategory)
                                           select new Workout
                                           {
                                               Id = workout.Id,
                                               LastModified = workout.LastModified,
                                               Repetitions = workout.Repetitions,
                                               WorkoutCategory = TransformWith<WorkoutCategoryDocumentType, WorkoutCategory>("WorkoutCategoryTransformer", workoutCategory).SingleOrDefault()
                                           };
        }
    }
}