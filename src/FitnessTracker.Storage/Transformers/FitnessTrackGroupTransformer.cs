using System.Linq;
using FitnessTracker.Infrastructure.Models;
using FitnessTracker.Storage.DocumentTypes;
using Raven.Client.Indexes;

namespace FitnessTracker.Storage
{
    public class FitnessTrackGroupTransformer : AbstractTransformerCreationTask<FitnessTrackGroupDocumentType>
    {
        public FitnessTrackGroupTransformer()
        {
            TransformResults = fitnessTrackGroups => from fitnessTrackGroup in fitnessTrackGroups
                                                     let workouts = Include<WorkoutDocumentType>(fitnessTrackGroup.Workouts)
                                                     select new FitnessTrackGroup
                                                     {
                                                         Id = fitnessTrackGroup.Id,
                                                         DateTracked = fitnessTrackGroup.DateTracked,
                                                         Workouts = TransformWith<WorkoutDocumentType, Workout>("WorkoutTransformer", workouts).ToList()
                                                     };
        }
    }
}