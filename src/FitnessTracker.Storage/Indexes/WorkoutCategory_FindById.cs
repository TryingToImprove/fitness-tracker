using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using FitnessTracker.Storage.DocumentTypes;

namespace FitnessTracker.Storage
{
    public class WorkoutCategory_FindById : AbstractIndexCreationTask<WorkoutCategoryDocumentType>
    {
        public WorkoutCategory_FindById()
        {
            Map = (workoutCategories) => from workoutCategory in workoutCategories
                                         select new WorkoutCategoryDocumentType
                                         {
                                             Id = workoutCategory.Id,
                                             Name = workoutCategory.Name
                                         };

            Index(x => x.Id, FieldIndexing.Analyzed);
        }
    }
}