using System;
using System.Linq;
using FitnessTracker.Infrastructure.Models;
using FitnessTracker.Storage.DocumentTypes;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace FitnessTracker.Storage
{
    public class Workout_FindByTrackingGroupId : AbstractIndexCreationTask<WorkoutDocumentType>
    {


        public Workout_FindByTrackingGroupId()
        {
            Map = (workouts) => from workout in workouts
                                select new WorkoutDocumentType
                                {
                                    Id = workout.Id,
                                    FitnessTrackGroupId = workout.FitnessTrackGroupId,
                                    WorkoutCategory = workout.WorkoutCategory,
                                    Repetitions = workout.Repetitions,
                                    LastModified = workout.LastModified
                                };

            Index(x => x.FitnessTrackGroupId, FieldIndexing.Analyzed);
        }
    }
}