using System.Linq;
using FitnessTracker.Storage.DocumentTypes;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace FitnessTracker.Storage
{
    public class FitnessTrackGroup_FindByIds : AbstractIndexCreationTask<FitnessTrackGroupDocumentType>
    {


        public FitnessTrackGroup_FindByIds()
        {
            Map = (fitnessTrackGroups) => from fitnessTrackGroup in fitnessTrackGroups
                                          select new FitnessTrackGroupDocumentType
                                          {
                                              Id = fitnessTrackGroup.Id,
                                              Workouts = fitnessTrackGroup.Workouts,
                                              DateTracked = fitnessTrackGroup.DateTracked
                                          };

            Index(x => x.Id, FieldIndexing.Analyzed);
        }
    }
}