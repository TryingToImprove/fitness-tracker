using System.Linq;
using FitnessTracker.Storage.DocumentTypes;
using Raven.Client.Indexes;

namespace FitnessTracker.Storage
{
    public class FitnessTrackGroup_FindAll : AbstractIndexCreationTask<FitnessTrackGroupDocumentType>
    {


        public FitnessTrackGroup_FindAll()
        {
            Map = (fitnessTrackGroups) => from fitnessTrackGroup in fitnessTrackGroups
                                          select new FitnessTrackGroupDocumentType
                                          {
                                              Id = fitnessTrackGroup.Id,
                                              Workouts = fitnessTrackGroup.Workouts,
                                              DateTracked = fitnessTrackGroup.DateTracked
                                          };
        }
    }
}