using System.Linq;
using FitnessTracker.Infrastructure.Models;
using FitnessTracker.Storage.DocumentTypes;
using Raven.Client.Indexes;

namespace FitnessTracker.Storage
{
    public class WorkoutCategory_FindAll : AbstractIndexCreationTask<WorkoutCategoryDocumentType>
    {


        public WorkoutCategory_FindAll()
        {
            Map = (workoutCategories) => from workoutCategory in workoutCategories
                                         select new WorkoutCategoryDocumentType
                                         {
                                             Id = workoutCategory.Id,
                                             Name = workoutCategory.Name
                                         };
        }
    }
}