using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Client.Linq;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Data;
using FitnessTracker.Infrastructure.Models;
using FitnessTracker.Storage.DocumentTypes;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Document;

namespace FitnessTracker.Storage
{
    public class PersistedFitnessStore : IFitnessStorage
    {
        private readonly IAsyncDocumentSession _documentSession;

        private static IDocumentStore DocumentStore;

        public PersistedFitnessStore(IAsyncDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        public async Task<FitnessTrackGroup> AddAsync(FitnessTrackGroup fitnessTrackGroup)
        {
            var workoutDocs = fitnessTrackGroup.Workouts.Select(x => new WorkoutDocumentType
            {
                LastModified = x.LastModified,
                Repetitions = x.Repetitions,
                WorkoutCategory = x.WorkoutCategory.Id
            });

            await Task.WhenAll(workoutDocs.Select(x => _documentSession.StoreAsync(x)));

            var fitnessTrackGroupDoc = new FitnessTrackGroupDocumentType
            {
                DateTracked = fitnessTrackGroup.DateTracked,
                Workouts = workoutDocs.Select(x => x.Id)
            };

            await _documentSession.StoreAsync(fitnessTrackGroupDoc);
            await _documentSession.SaveChangesAsync();

            return await FindAsync(fitnessTrackGroupDoc.Id);
        }

        public Task<IList<FitnessTrackGroup>> FindAllAsync()
        {
            return _documentSession.Query<FitnessTrackGroup, FitnessTrackGroup_FindAll>()
                        .TransformWith<FitnessTrackGroupTransformer, FitnessTrackGroup>()
                        .ToListAsync();
        }

        public Task<FitnessTrackGroup> FindAsync(string fitnessTrackGroupId)
        {
            return _documentSession.LoadAsync<FitnessTrackGroupTransformer, FitnessTrackGroup>(fitnessTrackGroupId);
        }

        public Task<IList<Workout>> FindWorkoutsAsync(string fitnessTrackGroupId)
        {
            return _documentSession.Query<WorkoutDocumentType, Workout_FindByTrackingGroupId>()
                        .Where(x => x.FitnessTrackGroupId == fitnessTrackGroupId)
                        .TransformWith<WorkoutTransformer, Workout>()
                        .ToListAsync();
        }

        public Task<IList<Workout>> FetchWorkoutsAsync(IEnumerable<string> workoutIds)
        {
            return _documentSession.Query<WorkoutDocumentType, Workout_FindById>()
                        .Where(x => workoutIds.Contains(x.Id))
                        .TransformWith<WorkoutTransformer, Workout>()
                        .ToListAsync();
        }

        public Task<long> GetNumberOfTracksForGroupAsync(string fitnessTrackGroupId)
        {
            return Task.FromResult(0l);
        }

        public async Task SaveWorkoutAsync(string fitnessTrackGroupId, Workout workout)
        {
            var workoutDocs = await _documentSession.Query<WorkoutDocumentType, Workout_FindByTrackingGroupId>()
                                        .Where(x => x.FitnessTrackGroupId == fitnessTrackGroupId)
                                        .ToListAsync();

            var workoutDoc = workoutDocs.SingleOrDefault(x => x.WorkoutCategory == workout.WorkoutCategory.Id);

            if (workoutDoc != null)
            {
                workoutDoc.LastModified = workout.LastModified;
                workoutDoc.Repetitions = workout.Repetitions;
            }
            else
            {
                workoutDoc = new WorkoutDocumentType
                {
                    FitnessTrackGroupId = fitnessTrackGroupId,
                    WorkoutCategory = workout.WorkoutCategory.Id,
                    LastModified = workout.LastModified,
                    Repetitions = workout.Repetitions
                };

                await _documentSession.StoreAsync(workoutDoc);
            }

            await Task.WhenAll(_documentSession.SaveChangesAsync(), DocumentStore.AsyncDatabaseCommands.PatchAsync(fitnessTrackGroupId, new[]{
                new PatchRequest {
                    Type = PatchCommandType.Add,
                    Name = "Workouts",
                    Value = workoutDoc.Id
                }
            }));

            workout.Id = workoutDoc.Id;
        }

        public Task UpdateAsync(FitnessTrackGroup fitnessTrackGroup)
        {
            throw new NotImplementedException();
        }

        public static PersistedFitnessStore Create(string url, string database)
        {
            if (DocumentStore == null)
            {
                DocumentStore = new DocumentStore { Url = url, DefaultDatabase = database };
                DocumentStore.Conventions.IdentityPartsSeparator = "-";
                DocumentStore.Initialize();

                Task.WaitAll(
                     DocumentStore.ExecuteIndexAsync(new Workout_FindByTrackingGroupId()),
                     DocumentStore.ExecuteIndexAsync(new FitnessTrackGroup_FindAll()),
                     DocumentStore.ExecuteIndexAsync(new FitnessTrackGroup_FindByIds()),
                     DocumentStore.ExecuteIndexAsync(new WorkoutCategory_FindAll()),
                     DocumentStore.ExecuteIndexAsync(new WorkoutCategory_FindById()),
                     DocumentStore.ExecuteIndexAsync(new Workout_FindById()),

                     //Transformers
                     DocumentStore.ExecuteTransformerAsync(new FitnessTrackGroupTransformer()),
                     DocumentStore.ExecuteTransformerAsync(new WorkoutCategoryTransformer()),
                     DocumentStore.ExecuteTransformerAsync(new WorkoutTransformer())
                 );
            }

            return new PersistedFitnessStore(DocumentStore.OpenAsyncSession());
        }

        public Task<IList<FitnessTrackGroup>> FetchAsync(IEnumerable<string> fitnessTrackGroupIds)
        {
            return _documentSession.Query<FitnessTrackGroup, FitnessTrackGroup_FindByIds>()
                        .TransformWith<FitnessTrackGroupTransformer, FitnessTrackGroup>()
                        .Where(x => fitnessTrackGroupIds.Contains(x.Id))
                        .ToListAsync();
        }


        public Task<IList<WorkoutCategory>> FindWorkoutCategoriesAsync()
        {
            return _documentSession.Query<WorkoutCategoryDocumentType, WorkoutCategory_FindAll>()
                    .TransformWith<WorkoutCategoryTransformer, WorkoutCategory>()
                    .ToListAsync();
        }

        public Task<IList<WorkoutCategory>> FetchWorkoutCategoriesAsync(IEnumerable<string> workoutIds)
        {
            return _documentSession.Query<WorkoutCategoryDocumentType, WorkoutCategory_FindById>()
                    .TransformWith<WorkoutCategoryTransformer, WorkoutCategory>()
                    .Where(x => workoutIds.Contains(x.Id))
                    .ToListAsync();
        }

        public async Task<WorkoutCategory> FindWorkoutCategoryAsync(string id)
        {
            var workoutCategory = await _documentSession.LoadAsync<WorkoutCategoryTransformer, WorkoutCategory>(id);

            return workoutCategory;
        }
    }
}
