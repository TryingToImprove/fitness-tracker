using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessTracker.Infrastructure.Models;
using Newtonsoft.Json;
using StackExchange.Redis;
using Xunit;

namespace FitnessTracker.Data.Tests
{
    // see example explanation on xUnit.net website:
    // https://xunit.github.io/docs/getting-started-dnx.html
    public class FitnessTrackRepositoryTests : IDisposable
    {
        private readonly ConnectionMultiplexer _connection;

        public FitnessTrackRepositoryTests()
        {
            var connection = ConnectionMultiplexer.Connect("localhost,allowAdmin=true");

            foreach (var endpoint in connection.GetEndPoints())
            {
                var server = connection.GetServer(endpoint);
                server.FlushAllDatabases();
            }

            _connection = connection;
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        CachedFitnessStorage cachedFitnessStorage;

        [Fact]
        public async Task CachedFitnessStorage_IsAbleToCreateANewFitnessGroup()
        {
            var storage = new CachedFitnessStorage(_connection.GetDatabase());
            var fitnessTrackGroup = new FitnessTrackGroup
            {
                Id = "fitnessTrackGroup-id"
            };

            await storage.AddAsync(fitnessTrackGroup);

            var fitnessTrackGroupCachedList = _connection.GetDatabase().ListRange("FitnessTrackGroups");
            Assert.NotNull(fitnessTrackGroupCachedList);
            Assert.Contains(fitnessTrackGroup.Id, fitnessTrackGroupCachedList);

            var fitnessTrackGroupCached = _connection.GetDatabase().StringGet(fitnessTrackGroup.Id);
            Assert.NotNull(fitnessTrackGroupCached);
        }
        /*
                [Fact]
                public Task CachedFitnessStorage_IsAbleToAddWorkout()
                {
                    var database = _connection.GetDatabase();
                    var fitnessTrackGroup = new FitnessTrackGroup
                    {
                        Id = "fitnessTrackGroup-id",
                        DateTracked = DateTime.Now,
                        Workouts = new List<Workout>()
                    };

                    database.StringSet(fitnessTrackGroup.Id, JsonConvert.SerializeObject(fitnessTrackGroup));

        var workout = new Workout{}
                    var storage = new CachedFitnessStorage(_connection.GetDatabase());
                    storage.SaveWorkoutAsync(fitnessTrackGroup.Id, )
                }
                */
    }
}
